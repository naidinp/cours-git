# Apprendre à utiliser git
Dès aujourd'hui commencez à sauvegarder facilement cotre travail et 
tout l'historique de vos modifications avec ce outil gratuit.

## Exemples issus du cours
Ce dépôt est utilise dans le cours pour présenter et expliquer l'utilisation de git

# Sommaire
* [Installation](Install.md)
* [Travailler au quotidien](Daily.md)